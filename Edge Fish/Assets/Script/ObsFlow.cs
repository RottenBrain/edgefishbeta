﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsFlow : MonoBehaviour {
    private GameObject _player;
    private int _flowType; // 1:FairFlow, 2:RevFlow

	void Start () {
        _player = GameObject.Find("Player");
        StartCoroutine(flowCor());
	}

    IEnumerator flowCor()
    {
        StartCoroutine(locateFlow());
        yield return StartCoroutine(appearAni());
        StartCoroutine(removeTimer());
        while(true){
            if (_player == null)
                break;
            affectFlow();
            yield return null;
        }
    }

    IEnumerator appearAni()
    {
        for (int i = 0; i < 10; i++){
            transform.localScale += new Vector3(0, 1, 1);
            yield return null;
        }
    }

    IEnumerator removeTimer()
    {
        yield return new WaitForSeconds(1.5f);
        Destroy(this.gameObject);
    }

    IEnumerator locateFlow()
    {
        while(true){
            if (_player.transform.position.x - transform.position.x > 100)
                transform.position = new Vector3(_player.transform.position.x + 1500, transform.position.y, 0);
            yield return null;
        }
    }

    public void affectFlow()
    {
		if (Mathf.Abs(transform.position.y - _player.transform.position.y) < 200)
		{
			Debug.Log("In Fair Flow" + _player.GetComponent<PlayerController>().Velocity);
			if (_flowType == 1)
			{
				_player.GetComponent<PlayerController>().inFairFlow();
			}
			else if (_flowType == 2)
			{
				_player.GetComponent<PlayerController>().inRevFlow();
			}
		}
    }

    public int FlowType{
        get { return _flowType; }
        set { _flowType = value; }
    }
}
