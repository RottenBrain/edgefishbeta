﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatusController : MonoBehaviour {

    public Text Nickname;
    public Text Level;
    public Text Exp;
    public Text Coin;

	// Use this for initialization
	void Start () {
        LoadStatusInfo();

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LoadStatusInfo(){
        Nickname.text = DataController.Instance.uinf.Nickname;
        Level.text = DataController.Instance.usta.Level.ToString();
        Exp.text = DataController.Instance.usta.Exp.ToString();
        Coin.text = DataController.Instance.usta.Coin.ToString();
    }
}
