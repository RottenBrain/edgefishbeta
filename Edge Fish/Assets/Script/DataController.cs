﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

//Filestream 
using System.IO;

//BinaryFormatter 등등
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


public class DataController : MonoBehaviour
{
    private static BinaryFormatter binFormatter = new BinaryFormatter();
    private static DataController _instance = null;
    public static DataController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(DataController)) as DataController;
                if (_instance == null)
                    Debug.LogError("Cannot find DataController.");
            }
            return _instance;
        }
    }


    public UserInform uinf;// = new UserInform();
    public UserStatus usta;// = new UserStatus();

    public const int FISHCOUNT = 1;
    public readonly Fish[] fish = { new Fish("Zacco", 20, 5, 200) };


    void Awake()
    {
        uinf = new UserInform();
        usta = new UserStatus();
        DontDestroyOnLoad(gameObject);
    }

    public static string pathForDocumentsFile(string filename)
    {
        if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            string path = Application.dataPath.Substring(0, Application.dataPath.Length - 5);
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(Path.Combine(path, "Documents"), filename);
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            string path = Application.persistentDataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }
        else
        {
            string path = Application.dataPath;
            path = path.Substring(0, path.LastIndexOf('/'));
            return Path.Combine(path, filename);
        }
    }



    //Serialized in [uinf.rba]
    [Serializable]
    public class UserInform
    {
        int _id;
        String _subid_G;
        String _subid_F;
        String _subid_N;
        String _nickname;

        public UserInform()
        {
            this._id = -1;
        }

        public Boolean load()
        {
            if (File.Exists(pathForDocumentsFile(Application.persistentDataPath + "/uinf.rba")))
            {
                FileStream fs = File.Open((Application.persistentDataPath + "/uinf.rba"), FileMode.Open);
                UserInform tmp = (UserInform)binFormatter.Deserialize(fs);

                this._id = tmp._id;
                this._subid_G = tmp._subid_G;
                this._subid_F = tmp._subid_F;
                this._subid_N = tmp._subid_N;
                this._nickname = tmp._nickname;

                fs.Close();

                return true;
            }

            else
            {
                Debug.Log("No uinf file. So it was created.");
                FileStream fs = File.Create(Application.persistentDataPath + "/uinf.rba");
                binFormatter.Serialize(fs, this);

                return false;
            }
        }

        public Boolean isIdSet()
        {
            if (this._id == -1)
                return false;
            else
                return true;
        }

        public int Id
        {
            get { return _id; }
            set { this._id = value; }
        }

        public String Nickname
        {
            get { return _nickname; }
            set { this._nickname = value; }
        }

        public void save()
        {
            FileStream fs = File.Create(Application.persistentDataPath + "/uinf.rba");
            binFormatter.Serialize(fs, this);
        }

        public Boolean isInit()
        {
            if (this._id == -1)
                return false;
            else
                return true;
        }

    }

    //Serialized in [usta.rba]
    [Serializable]
    public class UserStatus
    {
        int _level;
        int _exp;
        int _coin;

        //		DATE lastLogin;

        int _selectedFish;
        int playCount;
        ulong totalScore;

        Ability userAbility;
        PlayLog bestScore, bestVelocity, bestFlow;
        UserFishStatus[] userFishStatus = new UserFishStatus[FISHCOUNT];

        public UserStatus()
        {
        }

        public Boolean load()
        {
            if (File.Exists(pathForDocumentsFile(Application.persistentDataPath + "/usta.rba")))
            {
                FileStream fs = File.Open((Application.persistentDataPath + "/usta.rba"), FileMode.Open);
                UserStatus tmp = (UserStatus)binFormatter.Deserialize(fs);

                this._level = tmp._level;
                this._exp = tmp._exp;
                this._coin = tmp._coin;
                this.playCount = tmp.playCount;
                this.totalScore = tmp.totalScore;
                this._selectedFish = tmp._selectedFish;

                this.userAbility = tmp.userAbility;

                this.bestScore = tmp.bestScore;
                this.bestVelocity = tmp.bestVelocity;
                this.bestFlow = tmp.bestFlow;

                this.userFishStatus = tmp.userFishStatus;

                fs.Close();

                return true;
            }

            else
            {
                Debug.Log("No usta file. So it was created.");
                FileStream fs = File.Create(Application.persistentDataPath + "/usta.rba");
                binFormatter.Serialize(fs, this);

                return false;
            }
        }

        public int Level
        {
            get { return _level; }
            set { this._level = value; }
        }

        public int Exp
        {
            get { return _exp; }
            set { this._exp = value; }
        }

        public int Coin
        {
            get { return _coin; }
            set { this._coin = value; }
        }

        public void save()
        {
            FileStream fs = File.Create(Application.persistentDataPath + "/usta.rba");
            binFormatter.Serialize(fs, this);
        }

        public int SelectedFish
        {
            get { return _selectedFish; }
            set { this._selectedFish = value; }
        }





        [Serializable]
        class Ability
        {
            int velocity;
            int hpEfficiency;
            int flowResearch;
            int airResearch;

        }

        [Serializable]
        class PlayLog // 게임플레이정보를 판당 기준 통으로 관리, 서버에 보내짐.
        {
            int id;
            //      DATE time;

            ulong score;
            float highVelocity;
            float flowTime;
        }

        [Serializable]
        class UserFishStatus  // 배열 인덱스마다 물고기 지칭
        {
            bool isOwn;
            bool inBowl;
            int velocityUp;
            int resistUp;
            int hpUp;
            int hpEffciencyUp;
        }
    }

    [Serializable]
    public class Fish  // 배열 인덱스마다 물고기 지칭, 물고기에 대한 const data
    {
        String _name;
        int _hp;
        float _velocity;
        int _price;

        public Fish(String name, int hp, float velocity, int price)
        {
            this._name = name;
            this._hp = hp;
            this._velocity = velocity;
            this._price = price;
        }

        public String Name { get { return _name; } }
        public int Hp { get { return _hp; } }
        public float Velocity { get { return _velocity; } }
        public int Price { get { return _price; } }
    }


}


