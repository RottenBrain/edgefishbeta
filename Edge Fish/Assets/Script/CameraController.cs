﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private bool isFollow = false;


    private Vector3[] pos;

    private Vector3 relPosition;
    private Vector3 rotation;


    private GameObject _player;

    void Start()
    {
        _player = GameObject.Find("Player");

        pos = new Vector3[] { new Vector3 ( 0, 0, -150 ),
                              new Vector3 ( 400, 200, -500 ),
                              new Vector3 ( 800, 0, -1000 ) };
    }

    void LateUpdate()
    {
        if (isFollow)
            transform.position = _player.transform.position + relPosition + farViewByVelocity();

    }

    public IEnumerator cameraIntroSet()
    {
        for (int i = 0; i < 90; i++)
        {
            transform.position = pos[0] * Mathf.Cos(i * Mathf.Deg2Rad) + pos[1] * (1 - Mathf.Cos(i * Mathf.Deg2Rad));
            yield return null;
        }
    }

    public IEnumerator cameraFollowFish(){
        isFollow = true;
        for (float i = 0; i <= 90; i++)
        {
            relPosition = Vector3.Lerp(pos[1], pos[2] + farViewByVelocity(), (i / 90f));
            yield return null;
        }
    }

    public Vector3 farViewByVelocity(){
        float v = _player.GetComponent<PlayerController>().Velocity;

        if (v > 190)
            v = 190;

        return new Vector3(8, 0, -10) * _player.GetComponent<PlayerController>().Velocity * 0.1f;
    }

}
