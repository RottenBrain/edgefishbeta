﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

    public AudioSource BgmSource;
    public static AudioController instance = null;
    public bool isOn = false;

    // Use this for initialization
    void Start () {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(BgmSource);

        DontDestroyOnLoad(BgmSource);
	}

    public void OnClickedMuteButton()
    {
        if (instance.BgmSource.mute == isOn)
            instance.BgmSource.mute = !isOn;
        else
            instance.BgmSource.mute = isOn;
    }
}
