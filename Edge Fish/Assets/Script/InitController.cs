﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InitController : MonoBehaviour {

    public GameObject NicknamePanel;
    public InputField NicknameField;

	// Use this for initialization
	void Start () {
        StartCoroutine("initDC", null);
	}


	IEnumerator initDC()
	{
        //0% Need to check version
        //DataController.Instance.uinf = new DataController.Instance.UserInform();
        DataController.Instance.uinf.load();
        if (DataController.Instance.uinf.isIdSet() == false)
        {
            NicknamePanel.SetActive(true);
            Debug.Log("Making Nickname, giving id");
        }
		yield return new WaitForSeconds(1.0f);
		//Load UserInformation from sr file. 10%

		//DataController.Instance.usta = new DataController.Instance.UserStatus();
		DataController.Instance.usta.load();
		yield return null;
        //Load UserStatus from sr file. 30%


        //Compare with DB data. 80%

        //SceneManager.LoadScene("MainScene", LoadSceneMode.Single);
	}

    public void OnClikedCheckButton()
    {
        //Compare with DB data(Nickname)
        DataController.Instance.uinf.Nickname = NicknameField.text;

        //After set Nickname -> set Id
        DataController.Instance.uinf.Id = 0;

        DataController.Instance.uinf.save();

        Debug.Log("Success Init");
        SceneManager.LoadScene("01", LoadSceneMode.Single);
    }
}
