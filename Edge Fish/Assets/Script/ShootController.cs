﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootController : MonoBehaviour {
	private bool mouseState = false;
	private Queue<Vector3> posList;
    private Vector3[] posArr;


	// Use this for initialization
	void Start () {
		posList = new Queue<Vector3>();
	}
	
	// Update is called once per frame 
	void Update () {
		if (Input.GetMouseButtonDown(0))
		{
			mouseState = true;
		}

		else if (Input.GetMouseButtonUp(0))
		{
			mouseState = false;
			analyseShoot();
		}

		if (mouseState == true)
		{
			transform.position = Input.mousePosition * 1.2f;
			if(posList.Count >= 5)
				posList.Dequeue();
			posList.Enqueue(Input.mousePosition);
		}
	}

	private void analyseShoot()
	{
        posArr = posList.ToArray();
		GetComponent<PlayerController>().Velocity *= Vector3.Distance(posArr[0], posArr[4])/50;
		GetComponent<PlayerController>().Angle = Mathf.Atan((posArr[4].y - posArr[0].y) / (posArr[4].x - posArr[0].x)) * Mathf.Rad2Deg;

		GetComponent<PlayerController>().enabled = true;

		this.enabled = false;
	}
}
