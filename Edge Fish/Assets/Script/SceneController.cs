﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour {

    public GameObject InfoPanel;
    public GameObject BowlPanel;
    public GameObject AchePanel;
    public GameObject RankPanel;
    public GameObject CoinPanel;
    public GameObject CashPanel;
    public bool onPanel = true;

    public GameObject SettingPanel;
    public bool OnSet = false;

    // Use this for initialization
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void LoadMainScene()
    { SceneManager.LoadScene("01", LoadSceneMode.Single); }

    public void LoadPlayScene()
    { SceneManager.LoadScene("PlayScene", LoadSceneMode.Single); }

    public void LoadInfoScene()
    { SceneManager.LoadScene("02", LoadSceneMode.Single); }

    public void LoadShopScene()
    { SceneManager.LoadScene("03", LoadSceneMode.Single); }

    public void OnSettingButtonClicked()
    {
        SettingPanel.SetActive(!OnSet);
        OnSet = !OnSet;
    }



    public void OnInfoButtonClicked()
    {
        InfoPanel.SetActive(onPanel);
        BowlPanel.SetActive(!onPanel);
        AchePanel.SetActive(!onPanel);
        RankPanel.SetActive(!onPanel);
    }

    public void OnBowlButtonClicked()
    {
        InfoPanel.SetActive(!onPanel);
        BowlPanel.SetActive(onPanel);
        AchePanel.SetActive(!onPanel);
        RankPanel.SetActive(!onPanel);
    }

    public void OnAcheButtonClicked()
    {
        InfoPanel.SetActive(!onPanel);
        BowlPanel.SetActive(!onPanel);
        AchePanel.SetActive(onPanel);
        RankPanel.SetActive(!onPanel);
    }

    public void OnRankButtonClicked()
    {
        InfoPanel.SetActive(!onPanel);
        BowlPanel.SetActive(!onPanel);
        AchePanel.SetActive(!onPanel);
        RankPanel.SetActive(onPanel);
    }

    public void OnCoinButtonClicked()
    {
        CoinPanel.SetActive(onPanel);
        CashPanel.SetActive(!onPanel);
    }

    public void OnCashButtonClicked()
    {
        CoinPanel.SetActive(!onPanel);
        CashPanel.SetActive(onPanel);
    }
}
