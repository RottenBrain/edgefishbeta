﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayController : MonoBehaviour {
    int _state;
    GameObject _player;
    GameObject _camera;
    DataController.Fish defaultFish;
    //GameObject bg;

    Coroutine coCFref, coCCref, coCAref;

    Vector3 ObsDest, tmpNPos, tmpCPos;

    int _coin;


    void Awake () {
        _player = GameObject.Find("Player");
        _camera = GameObject.Find("Main Camera");
        defaultFish = DataController.Instance.fish[DataController.Instance.usta.SelectedFish];

        StartCoroutine("Play", null);
    }


	// Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {

	}

    IEnumerator Play(){
        loadFishBody();
        yield return null;

        yield return StartCoroutine(_camera.GetComponent<CameraController>().cameraIntroSet());

        _player.GetComponent<ShootController>().enabled = true;
        while (_player.GetComponent<ShootController>().enabled)
            yield return null;

        _player.GetComponent<PlayerController>().enabled = true;
        GameObject.Find("HpGauge").GetComponent<HPGaugeController>().enabled = true;
        StartCoroutine(_camera.GetComponent<CameraController>().cameraFollowFish());
        coCFref = StartCoroutine(CreateFlow());
        coCCref = StartCoroutine(CreateCoin());
        coCAref = StartCoroutine(CreateAir());
    }

    IEnumerator PlayEnd(){
        yield return null;
    }



    private void loadFishBody(){
		GameObject prefab = Resources.Load("fishBody/" + defaultFish.Name + "/" + defaultFish.Name) as GameObject;
		Debug.Log(prefab);
		GameObject fishBody = MonoBehaviour.Instantiate(prefab) as GameObject;

		fishBody.name = "FishBody"; // name을 변경
		fishBody.transform.parent = _player.transform;
//        fishBody.transform.scale = new Vector3(10, 10, 10);

		_player.GetComponent<PlayerController>().Velocity = defaultFish.Velocity;
		_player.GetComponent<PlayerController>().Hp = defaultFish.Hp;
	}




    IEnumerator CreateFlow(){
        float ratio = 2.0f;
        float freq = 5f;

        GameObject fairFlowRss = Resources.Load("Obstacle/FairFlow") as GameObject;
		GameObject revFlowRss = Resources.Load("Obstacle/RevFlow") as GameObject;

        GameObject flow;

		while(true){ // need condition
            yield return new WaitForSeconds(freq);

			if (Random.Range(0, 2f) < ratio)
			{
                flow = MonoBehaviour.Instantiate(fairFlowRss) as GameObject;
                flow.GetComponent<ObsFlow>().FlowType = 1;
            }
            else
            {
                flow = MonoBehaviour.Instantiate(revFlowRss) as GameObject;
				flow.GetComponent<ObsFlow>().FlowType = 2;
			}

            flow.transform.position = _player.transform.position + new Vector3(0, Random.Range(-800, 800), 0);

            if (freq > 3f)
                freq *= 0.95f;

            if (ratio > 0.8f)
                ratio *= 0.9f;
            
        }
    }

    IEnumerator CreateCoin(){
        int amount = 1;
        float freq = 3f;

        GameObject coinRss = Resources.Load("Obstacle/Coin") as GameObject;

        GameObject coin;

        while(true){
            yield return new WaitForSeconds(Random.Range(0, freq));

            coin = MonoBehaviour.Instantiate(coinRss) as GameObject;
            coin.transform.position = getObsPos();
            coin.GetComponent<ObsCoin>().Amount = amount;
            amount++;
        }
    }

    IEnumerator CreateAir(){
        float freq = 1f;

        GameObject airRss = Resources.Load("Obstacle/Air") as GameObject;
        GameObject air;

        while(true){
            yield return new WaitForSeconds(Random.Range(0, freq));

            air = MonoBehaviour.Instantiate(airRss) as GameObject;
            air.transform.position = getObsPos();
        }
    }

    public void playEnd(){
        // PlayLog playResult = new PlayLog(id, _player.transform.position.x(distance), Coin, highVelocity, flowTime); 
        // good;


        _player.SetActive(false);


        Destroy(GameObject.Find("HpGauge"));
		StopCoroutine(_camera.GetComponent<CameraController>().cameraFollowFish());
        StopCoroutine(coCFref);
        StopCoroutine(coCCref);
        StopCoroutine(coCAref);


        // create playLog, best things update;
        // serialize somthing;
        // give exp, coin;
        // distance;
    }



    public int State{
        get { return _state; }
        set { _state = value; }
    }

    public int Coin{
        get { return _coin; }
        set { _coin = value; }
    }

    private Vector3 getObsPos(){
		tmpNPos = _player.GetComponent<PlayerController>().calcNextPos();
		tmpCPos = _player.transform.position;
        ObsDest = tmpCPos + makeUnitVector(tmpCPos, tmpNPos) * 5000;

        return ObsDest + new Vector3(Random.Range(-1000f, 1000f), Random.Range(-1000f, 1000f), 0);
    }

    private Vector3 makeUnitVector(Vector3 start, Vector3 end){
        return (end - start) * 1 / Vector3.Distance(start, end);
    }
}
