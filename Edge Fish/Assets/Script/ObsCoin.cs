﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsCoin : MonoBehaviour
{
    GameObject _player;
    PlayController _playScript;
    int _amount;

    void Start()
    {
        _player = GameObject.Find("Player");
        _playScript = GameObject.Find("PlayController").GetComponent<PlayController>();

        StartCoroutine(removeTimer());
    }
    void Update()
    {
		if (_player == null)
			Destroy(this);

        if (Vector3.Distance(transform.position, _player.transform.position) < 80f)
        {
            _playScript.Coin += _amount;
            Debug.Log(_playScript.Coin);
            Destroy(this.gameObject);
        }

		if (transform.position.x + 200f < _player.transform.position.x)
		{
			Destroy(this.gameObject);
		}
    }



    IEnumerator removeTimer()
    {
        yield return new WaitForSeconds(15.0f);
        Destroy(this.gameObject);
    }


    public int Amount{
        get { return _amount; }
        set { _amount = value; }
    }
}
