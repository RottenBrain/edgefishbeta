﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObsAir : MonoBehaviour {
	private GameObject _player;

	void Start()
	{
		_player = GameObject.Find("Player");
        StartCoroutine(removeTimer());
	}
	void Update()
	{
        if (_player == null)
            Destroy(this);

		if (transform.position.x + 200f < _player.transform.position.x)
		{
			Destroy(this.gameObject);
		}

		if (Vector3.Distance(transform.position, _player.transform.position) < 80f)
		{
			_player.GetComponent<PlayerController>().Hp += 1;
			Destroy(this.gameObject);
		}


	}

	IEnumerator removeTimer()
	{
		yield return new WaitForSeconds(15.0f);
		Destroy(this.gameObject);
	}
}
