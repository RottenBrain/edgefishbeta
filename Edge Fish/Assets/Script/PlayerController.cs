﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
	float _velocity;
	float _angle;
	float _hp;

	public Vector3 nextPos;

	private int rotState;

	// Use this for initialization
	void Start()
	{

		rotState = 0;
	}

	// Update is called once per frame
	void Update()
	{
        if (transform.position.x < -100){
            this.enabled = false;
        }

        naturalChange();
		nextPos = calcNextPos();
		updateRot();

		transform.position = nextPos;

        if (transform.position.y < -100f)
            GameObject.Find("PlayController").GetComponent<PlayController>().playEnd();
	}


	private void naturalChange()
	{
		naturalChangeVelocity();
		naturalChangeAngle();
	}

	private void updateRot()
	{
		float rotZ = rotFR();
		float rotXY = rotLR();

		Vector3 newRot = new Vector3(rotXY * Mathf.Cos(rotZ * Mathf.Deg2Rad), rotXY * Mathf.Sin(rotZ * Mathf.Deg2Rad) + 90, rotZ);
		transform.rotation = Quaternion.Euler(newRot);

		rotState += 2;

		if (rotState >= 120)
			rotState = 0;
	}

	private float rotLR()
	{
		if (rotState < 30)
			return rotState / 2;
		else if (rotState < 90)
			return 30 - (rotState / 2);
		else
			return (rotState / 2) - 60;
	}

	private float rotFR()
	{
		return Mathf.Atan((nextPos.y - transform.position.y) / (nextPos.x - transform.position.x)) * Mathf.Rad2Deg;
	}

	public Vector3 calcNextPos()
	{
		Vector3 result = transform.position;
		result.x += Mathf.Cos(Angle * Mathf.Deg2Rad) * Velocity;
		result.y += Mathf.Sin(Angle * Mathf.Deg2Rad) * Velocity;

		return result;
	}


	private void naturalChangeVelocity()
	{
		float c = Angle / 90 * -0.02f;
		Velocity += c * 1f;
	}

	private void naturalChangeAngle()
	{
		float c = Mathf.Abs(Angle) / 270;
		if (Angle > -88)
		{
			Angle -= 1f + c;
		}
	}


    public void inFairFlow(){
        Velocity *= 1.01f;
    }
    public void inRevFlow(){
        Velocity *= 0.98f;
    }




    public float Velocity
    {
        get { return _velocity; }
        set { _velocity = value; }
    }

    public float Hp
    {
        get { return _hp; }
        set { _hp = value; }
    }

    public float Angle
    {
        get { return _angle; }
        set { _angle = value; }
    }

}

