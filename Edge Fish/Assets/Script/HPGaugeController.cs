﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPGaugeController : MonoBehaviour {
	PlayerController playerCS;
    GameObject body;
    float hp;

	// Use this for initialization
	void Start () {
		playerCS = GameObject.Find("Player").GetComponent<PlayerController>();
        body = this.transform.GetChild(0).gameObject;
        hp = playerCS.Hp;

		body.transform.localScale += new Vector3(hp, 1, 1);
		this.transform.position += new Vector3(hp / 2, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKey("space") && playerCS.Hp > 0.05f)
		{

            if(playerCS.Angle < 87)
                playerCS.Angle += 3f;
            else
                playerCS.Angle = 89.99f;

			playerCS.Hp -= 0.05f;
			playerCS.Velocity += 0.2f;
		}

		if (hp != playerCS.Hp)
		{
            this.transform.position += new Vector3((playerCS.Hp - hp) / 2, 0, 0);
            body.transform.localScale += new Vector3((playerCS.Hp - hp), 0, 0);

            hp = playerCS.Hp;
		}

        if (hp < 0.05f && body.transform.localScale.y == 1)
            body.transform.localScale -= new Vector3(0, 1, 1);
        if (hp > 0.05f && body.transform.localScale.y == 0)
            body.transform.localScale += new Vector3(0, 1, 1);



	}
}
