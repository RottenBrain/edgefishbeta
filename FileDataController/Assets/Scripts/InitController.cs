﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InitController : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        StartCoroutine("initDC", null);
    }


    IEnumerator initDC()
    {
        DataController.Instance.uinf.load();
        if (DataController.Instance.uinf.isIdSet() == false)
        {
            Debug.Log("Making Nickname, giving id");
        }
        yield return new WaitForSeconds(1.0f);
        DataController.Instance.usta.load();
        yield return null;
    }

    public void OnClikedCheckButton()
    {
        DataController.Instance.uinf.Nickname = "jongsen";
        DataController.Instance.uinf.Id = 0;
        DataController.Instance.uinf.save();

        DataController.Instance.usta.Level = 1;
        DataController.Instance.usta.Exp = 10;
        DataController.Instance.usta.Coin = 100;
        DataController.Instance.usta.save();

    }
}
